const axios = require("axios");
const cheerio = require('cheerio');
const fs = require('fs');
const dateFormat = require("dateformat");
const moment = require('moment');
const pLimit = require('p-limit');

const SITE = 'https://interns-blog.herokuapp.com';
const SITE_TOPIC = `${SITE}/v1/topic`;
const TOPIC_LIMIT = 10;
const LIMIT_SIMULTANEOUS_FILE_WRITE = 3;

const limit = pLimit(LIMIT_SIMULTANEOUS_FILE_WRITE);

(async () => {
    console.time('getTopicsUrl');

    let topicUrlArray = await getTopicsUrl(SITE_TOPIC);
    console.log(topicUrlArray);

    console.timeEnd('getTopicsUrl');

    for (let i = 0; i <= topicUrlArray.length; i++) {
        if (i >= TOPIC_LIMIT) break;

        console.log('adding topic url to queue:', topicUrlArray[i]);

        limit(async () => {
            console.log('START processing topic url:', i, topicUrlArray[i]);

            let topicJson = await getTopicJson(topicUrlArray[i]);

            topicJson = JSON.stringify(topicJson, null, 2);

            writeToFile(topicJson, i + 1);

            console.log('END processing topic url:', i, topicUrlArray[i]);
        });
    }
})();

function getContent(url) {
    return axios({
      method: 'get',
      url,
      responseType: 'text'
    });
}


async function getTopicsUrl(siteUrl) {
    const content = await getContent(siteUrl);
    const $ = cheerio.load(content.data);

    let topicUrlArray = [];

    $('h2.post-title').each(function (i, element) {
        const topicUrl = $(element).find('a').attr('href');

        if (!topicUrlArray.includes(`${SITE}${topicUrl}`)) {
            topicUrlArray[i] = `${SITE}${topicUrl}`;
        }
    });

    topicUrlArray = topicUrlArray.filter(element => element !== null);

    return topicUrlArray;
}

async function getTopicJson(url) {
    const content = await getContent(url);
    const $ = cheerio.load(content.data);
    const postsArray = [];

    $('section.message').each((i, element) => {
        const time = $(element).find('p.message-meta').find('span').text().split(' ');
        const postCreationDate = dateFormat(
            Date.now() - (Date.now() - moment().subtract(time[0], time[1])),
            'isoDate');
        const postAuthor = $(element).find('a.message-author').text().trim();
        const postText = $(element).find('div.message-description').text().trim();

        postsArray[i] = {
            author: postAuthor,
            date: postCreationDate,
            text: postText,
        }
    });

    return {
        title: $(".post-title").text().trim(),
        author: $(".post-meta a").text().trim(),
        description: $(".post-description > p").text().trim(),
        messages: postsArray
    };
}

function writeToFile(json, postNumber) {
    const fileName = `topics/topic${postNumber}.json`;

    fs.writeFile(
        fileName,
        json,
        "UTF-8",
        (err) => {
            if (err) {
                console.error(fileName, err);
            } else {
                console.log('file', fileName, 'saved');
            }
        }
    );
}
